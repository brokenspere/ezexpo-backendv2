package com.example.ezexpo.service

import com.example.ezexpo.entity.Visitor

interface VisitorService {
    fun save(visitor: Visitor, id: Long): Visitor
}