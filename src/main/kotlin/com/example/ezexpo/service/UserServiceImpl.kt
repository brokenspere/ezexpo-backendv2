package com.example.ezexpo.service

import com.example.ezexpo.dao.UserDao
import com.example.ezexpo.entity.BoothOwner
import com.example.ezexpo.entity.User
import com.example.ezexpo.entity.UserRole
import com.example.ezexpo.entity.Visitor
import com.example.ezexpo.entity.dto.displayUser
import com.example.ezexpo.repositoy.BoothOwnerRepository
import com.example.ezexpo.repositoy.UserRepositoryObj
import com.example.ezexpo.security.entity.Authority
import com.example.ezexpo.security.entity.AuthorityName
import com.example.ezexpo.security.entity.JwtUser
import com.example.ezexpo.security.repository.AuthorityRepository
import com.example.ezexpo.security.repository.UserRepository
import com.example.ezexpo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

import org.springframework.stereotype.Service
import java.lang.Enum
import javax.transaction.Transactional


@Service
class UserServiceImpl :UserService {



    @Autowired
    lateinit var userRepositoryObj: UserRepositoryObj
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var boothUserDao: BoothOwnerRepository
    override fun getBoothByUserId(id: Long): User? {
        return userDao.getBoothDetailByUserId(id)
    }

    override fun getUserByUsernameAndPassword(username: String, password: String): User? {
        return userDao.getUserByUsernameAndPassword(username, password)
    }

    @Transactional
    override fun update(user: displayUser): User? {

        val dbObj = user.id?.let { userDao.getBoothDetailByUserId(it) }

        if (user.username !=null){

            dbObj?.username = user.username
            dbObj?.jwtUser?.setUsername(user.username)
        }
        if(user.email !=null){

            dbObj?.email = user.email
        }
        if(user.password !=null){
            val encoder = BCryptPasswordEncoder()
            dbObj?.password = encoder.encode(user.password)
            dbObj?.jwtUser?.setPassword(encoder.encode(user.password))
        }
        if(user.firstName != null){
            dbObj?.firstName = user.firstName
        }
        if(user.lastName != null){

            dbObj?.lastName = user.lastName
        }

        return dbObj


    }
    override fun getUserByEmail(email: String): User? {
        return userDao.getUserByEmail(email)
    }


    override fun save(user: displayUser): User {
        val boothOwner = MapperUtil.INSTANCE.mapUserDto(user)
        return userDao.save(boothOwner)
    }
    override fun getAllUser(): List<User> {
        val role = "BOOTHOWNER"
        val roleEnum: UserRole = UserRole.valueOf(role)
        return userDao.getUserByRole(roleEnum)
    }
    override fun getUserByRole(role:String): List<User> {
//        val role = "BOOTHOWNER"
        val roleEnum: UserRole = UserRole.valueOf(role)
        return userDao.getUserByRole(roleEnum)
    }
    override fun getUserById(id: Long): User? {
       return userDao.getUserById(id)
    }
    override fun saveTest(user: BoothOwner): BoothOwner {
        return userDao.saveTest(user)
    }
}