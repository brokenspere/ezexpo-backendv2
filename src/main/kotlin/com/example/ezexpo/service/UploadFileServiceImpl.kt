package com.example.ezexpo.service

import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


@Service
class UploadFileServiceImpl {
    @Throws(IOException::class)
    private fun convertMultiPartToFile(file: MultipartFile): File {
        val convFile = File(file.originalFilename!!)
        val fos = FileOutputStream(convFile)
        fos.write(file.bytes)
        fos.close()
        return convFile
    }

    private fun generateFileName(multiPart: MultipartFile): String {
        return Date().time.toString() + "-" + multiPart.originalFilename!!.replace(" ", "_")
    }


    fun uploadFile(multipartFile: MultipartFile): MultipartFile {

        var fileUrl = ""
        try {
             convertMultiPartToFile(multipartFile)
             generateFileName(multipartFile)
            //uploadFileTos3bucket(fileName, file)
            //file.delete()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return multipartFile
    }
}

