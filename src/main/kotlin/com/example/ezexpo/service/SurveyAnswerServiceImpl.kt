package com.example.ezexpo.service

import com.example.ezexpo.dao.SurveyAnswerDao
import com.example.ezexpo.dao.SurveyQuestionDao
import com.example.ezexpo.entity.SurveyAnswer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional


@Service
class SurveyAnswerServiceImpl : SurveyAnswerService{

    @Autowired
    lateinit var surveyAnswerDao: SurveyAnswerDao
    @Autowired
    lateinit var surveyQuestionDao: SurveyQuestionDao


    @Transactional
    override fun save(answer: SurveyAnswer, id: Long): SurveyAnswer {
       val surveyQuestion = surveyQuestionDao.getSurveyById(id)
       val answer = surveyAnswerDao.save(answer)
        surveyQuestion?.surveyAnswer?.add(answer)
        return answer
    }


}