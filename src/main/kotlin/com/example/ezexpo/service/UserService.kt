package com.example.ezexpo.service

import com.example.ezexpo.entity.BoothOwner
import com.example.ezexpo.entity.User
import com.example.ezexpo.entity.UserRole
import com.example.ezexpo.entity.dto.displayUser
import java.lang.Enum

interface UserService {
     fun getBoothByUserId(id: Long): User?
     fun getUserByUsernameAndPassword(username:String,password:String): User?
     fun update(user: displayUser): User?
     fun getUserByEmail(email: String): User?
     fun save(user: displayUser):User
     fun getAllUser(): List<User>
     fun getUserById(id: Long): User?
     fun getUserByRole(role: String): List<User>
     fun saveTest(user: BoothOwner): BoothOwner 
}