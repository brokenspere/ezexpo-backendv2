package com.example.ezexpo.service

import com.example.ezexpo.entity.SurveyAnswer

interface SurveyAnswerService {
    fun save(answer: SurveyAnswer, id: Long): SurveyAnswer

}