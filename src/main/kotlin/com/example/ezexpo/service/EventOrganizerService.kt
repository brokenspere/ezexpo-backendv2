package com.example.ezexpo.service

import com.example.ezexpo.entity.EventOrganizer

interface EventOrganizerService {
    fun update(event: EventOrganizer): EventOrganizer?

}