package com.example.ezexpo.service

import com.example.ezexpo.dao.VisitorRegisterDao
import com.example.ezexpo.entity.VisitorRegister
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service

class VisitorRegisterServiceImpl : VisitorRegisterService{

    @Autowired
    lateinit var visitorRegisterDao: VisitorRegisterDao

    override fun getAllVisitor(): List<VisitorRegister> {
        return visitorRegisterDao.getAllvisitor()
    }
    override fun save(visitor: VisitorRegister): VisitorRegister {
        return visitorRegisterDao.save(visitor)
    }

}