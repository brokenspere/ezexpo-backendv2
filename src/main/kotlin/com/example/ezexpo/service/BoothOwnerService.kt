package com.example.ezexpo.service

import com.example.ezexpo.entity.BoothOwner
import com.example.ezexpo.entity.dto.BoothOwnerDtoNoVisitor
import com.example.ezexpo.entity.dto.displayUser

interface BoothOwnerService {
     //fun getBoothByName(boothName:String): BoothOwner?
     fun getAllBoothDetail(): List<BoothOwner>

     fun getBoothDetailByName(boothName: String): BoothOwner?

     fun getBoothDetailById(id: Long): BoothOwner?

     //fun save(booth: BoothOwner): BoothOwner

}