package com.example.ezexpo.service

import com.example.ezexpo.entity.VisitorRegister

interface VisitorRegisterService {
    fun getAllVisitor(): List<VisitorRegister>
    fun save(visitor: VisitorRegister): VisitorRegister
}