package com.example.ezexpo.service

import com.example.ezexpo.entity.EventOrganizer
import com.example.ezexpo.repositoy.EventOrganizerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class EventOrganizerServiceImpl:EventOrganizerService {
    @Autowired
    lateinit var eventOrganizerRepository: EventOrganizerRepository


    @Transactional
    override fun update(event: EventOrganizer): EventOrganizer? {
        val dbObj = event.id?.let { eventOrganizerRepository.findById(it).orElse(null) }

        if(event.announcement !=null){
           dbObj?.announcement = event.announcement
        }
        return dbObj

    }

}