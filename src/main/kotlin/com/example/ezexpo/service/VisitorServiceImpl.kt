package com.example.ezexpo.service

import com.example.ezexpo.dao.BoothOwnerDao
import com.example.ezexpo.dao.VisitorDao
import com.example.ezexpo.entity.Visitor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class VisitorServiceImpl: VisitorService{
    @Autowired
    lateinit var boothOwnerDao: BoothOwnerDao

    @Autowired
    lateinit var visitorDao: VisitorDao

    @Transactional
    override fun save(visitor: Visitor, id: Long): Visitor {
        val boothOwn = boothOwnerDao.getBoothDetailById(id)
        val visitor = visitorDao.save(visitor)
        boothOwn?.visitor?.add(visitor)
        return visitor
    }

}