package com.example.ezexpo.service

import com.example.ezexpo.dao.BoothOwnerDao
import com.example.ezexpo.dao.UserDao
import com.example.ezexpo.entity.BoothOwner
import com.example.ezexpo.entity.dto.BoothOwnerDtoNoVisitor
import com.example.ezexpo.entity.dto.displayUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class BoothOwnerServiceImpl : BoothOwnerService{



    @Autowired
    lateinit var boothOwnerDao: BoothOwnerDao

    @Autowired
    lateinit var userDao: UserDao

//
//    override fun getBoothByName(boothName: String): BoothOwner? {
//       return  boothOwnerDao.getBoothDetailByName(boothName)
//    }

    override fun getAllBoothDetail(): List<BoothOwner> {
        return boothOwnerDao.getAllBoothDetail()
    }
    override fun getBoothDetailByName(boothName: String): BoothOwner? {
        return boothOwnerDao.getBoothDetailByName(boothName)
    }
    override fun getBoothDetailById(id: Long): BoothOwner? {
      return boothOwnerDao.getBoothDetailById(id)
    }




}