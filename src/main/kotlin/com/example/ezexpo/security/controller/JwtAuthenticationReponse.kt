package com.example.ezexpo.security.controller

class JwtAuthenticationReponse {
    data class JwtAuthenticationResponse(
            var token: String? = null
    )
}