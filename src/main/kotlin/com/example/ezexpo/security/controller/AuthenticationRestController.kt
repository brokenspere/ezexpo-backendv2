package com.example.ezexpo.security.controller

import com.amazonaws.services.mq.model.UnauthorizedException
import com.example.ezexpo.entity.User
import com.example.ezexpo.security.JwtTokenUtil
import com.example.ezexpo.security.entity.JwtUser
import com.example.ezexpo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.mobile.device.Device
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.xml.ws.http.HTTPException

@RestController
class AuthenticationRestController{
    @Value("\${jwt.header}")
    private val tokenHeader: String? = null

    @Autowired
    lateinit var authenticationManager: AuthenticationManager

    @Autowired
    lateinit var jwtTokenUtil: JwtTokenUtil

    @Autowired
    lateinit var userDetailsService: UserDetailsService


    @PostMapping("\${jwt.route.authentication.path}")
    @Throws(AuthenticationException::class)
    fun createAuthenticationToken(@RequestBody authenticationRequest: JwtAuthenticationRequest, device: Device): ResponseEntity<*> {

        if(authenticationRequest.username == null || authenticationRequest.password == null){
            return ResponseEntity.ok("invalid format")
        }
        // Perform the security
        val authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                        authenticationRequest.username,
                        authenticationRequest.password)
        )
            SecurityContextHolder.getContext().authentication = authentication

        // Reload password post-security so we can generate token
        val userDetails = userDetailsService.loadUserByUsername(authenticationRequest.username)
        val token = jwtTokenUtil.generateToken(userDetails, device)
        val result = HashMap<String,Any>()
        result.put("token", token)

        try{
            val currentUser = (userDetails as JwtUser).user
            currentUser?.firstName?.let { result.put("firstName", it) }
            currentUser?.lastName?.let { result.put("lastName", it) }
            currentUser?.username?.let { result.put("username", it) }
            currentUser?.password?.let { result.put("password", it) }
            currentUser?.email?.let { result.put("email", it) }
            currentUser?.userRole?.let { result.put("userRole", it) }
            currentUser?.phoneNumber?.let{result.put("phoneNumber",it)}
            currentUser?.securityQuestion?.let{ result.put("securityQuestion",it)}
            currentUser?.securityAnswer?.let{ result.put("securityAnswer",it)}
            currentUser?.id?.let { result.put("id", it) }

            when(currentUser){
                is User -> currentUser?.firstName?.let { result.put("firstName", it) }
                is User -> currentUser?.lastName?.let { result.put("lastName", it) }
                is User -> currentUser?.username?.let { result.put("username", it) }
                is User -> currentUser?.password?.let { result.put("password", it) }
                is User -> currentUser?.email?.let { result.put("email", it) }
                is User -> currentUser?.phoneNumber?.let { result.put("phoneNumber",it) }
                is User -> currentUser?.securityQuestion?.let{ result.put("securityQuestion",it)}
                is User -> currentUser?.securityAnswer?.let{ result.put("securityAnswer",it)}
                is User -> currentUser?.userRole?.let { result.put("userRole", it) }
                is User->  currentUser?.id?.let { result.put("id", it) }
            }

            return ResponseEntity.ok(result)
        }catch (e: UnauthorizedException){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("hello world")
        }




    }


    @GetMapping("\${jwt.route.authentication.refresh}")
    fun refreshAndGetAuthenticationToken(request: HttpServletRequest): ResponseEntity<*> {
        val token = request.getHeader(tokenHeader)
        val username = jwtTokenUtil.getUsernameFromToken(token)
        val user = userDetailsService.loadUserByUsername(username) as JwtUser

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.lastPasswordResetDate)) {
            val refreshedToken = jwtTokenUtil.refreshToken(token)
            return ResponseEntity.ok(JwtAuthenticationReponse.JwtAuthenticationResponse(refreshedToken))
        } else {
            return ResponseEntity.badRequest().body<Any>(null)
        }
    }
}