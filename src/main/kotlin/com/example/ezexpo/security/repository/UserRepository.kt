package com.example.ezexpo.security.repository

import com.example.ezexpo.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser?
}