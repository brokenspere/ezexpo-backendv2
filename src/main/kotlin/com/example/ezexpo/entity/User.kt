package com.example.ezexpo.entity


import com.example.ezexpo.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
 class User (open var firstName:String? = null,
             open var lastName:String? = null,
             open var email: String? = null,
             open var username: String? = null,
             open var password: String? = null,
             open var phoneNumber: String?=null,
             open var securityQuestion: String? = null,
             open var securityAnswer: String? = null,
             open var userRole: UserRole? = null)

 {
     @Id
     @GeneratedValue
     var id:Long?=null

     @OneToOne
     var jwtUser:JwtUser?= null
//     @OneToMany
//     var visitor:MutableList<Visitor>? = mutableListOf<Visitor>()


}