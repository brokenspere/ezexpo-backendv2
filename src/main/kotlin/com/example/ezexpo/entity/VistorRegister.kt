package com.example.ezexpo.entity

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import javax.persistence.*

@Entity
data class VisitorRegister(
        var name : String? = null,
        var email : String? = null,
        var phoneNumber : String? = null,
        var registerTime: LocalDateTime? = null
)
{
    @Id
    @GeneratedValue
    var id:Long?=null
}