package com.example.ezexpo.entity

import java.time.LocalDate
import javax.persistence.*


@Entity
data class EventOrganizer(override var firstName: String? = null,
                          override var lastName: String? = null,
                          override var email: String? = null,
                          override var username: String? = null,
                          override var password: String? = null,
                          override var phoneNumber: String? = null,
                          override var securityQuestion: String? = null,
                          override var securityAnswer: String? = null,
                          override var userRole: UserRole? = UserRole.EVENTORGANIZER,
                          var announcement: String? = null
                        ): User (firstName, lastName , email,username,password,phoneNumber,securityQuestion,securityAnswer,userRole) {
    @OneToMany
    var surveyQuestion : MutableList<SurveyQuestion>? = mutableListOf()
    @OneToMany
    var visitorRegister: MutableList<VisitorRegister>? = mutableListOf()
}
