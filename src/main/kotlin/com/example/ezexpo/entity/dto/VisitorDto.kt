package com.example.ezexpo.entity.dto

import java.time.LocalDateTime

class VisitorDto ( var happy : Int? = null,
                   var sad : Int? = null,
                   var surprise : Int? = null,
                   var angry : Int ? = null,
                   var disgust : Int? = null,
                   var fear : Int? = null,
                   var netural:Int? = null,
                   var total:Int? = null,
                   var visitTime : LocalDateTime? = null)