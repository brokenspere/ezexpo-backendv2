package com.example.ezexpo.entity
import javax.persistence.*


@Entity

data class SurveyAnswer(var answer1:String?=null,
                        var answer2:String?=null,
                        var answer3:String?=null,
                        var answer4:String?=null,
                        var answer5:String?=null,
                        var sugesstion:String?=null) {
    @Id
    @GeneratedValue
    var id:Long?=null

}