package com.example.ezexpo.entity

import javax.persistence.*


@Entity
 data class SurveyQuestion(var surveyName:String?=null,
                           var surveyDescription:String?=null,
                           var question1:String?=null,
                           var question2:String?=null,
                           var question3:String?=null,
                           var question4:String?=null,
                           var question5:String?=null) {
    @Id
    @GeneratedValue
    var id:Long?=null

    @OneToMany
    var surveyAnswer : MutableList<SurveyAnswer>? = mutableListOf()
}