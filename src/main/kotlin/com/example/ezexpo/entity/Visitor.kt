package com.example.ezexpo.entity

import java.time.LocalDateTime
import javax.persistence.*

@Entity
 data class Visitor ( var happy : Int? = null,
                      var sad : Int? = null,
                      var surprise : Int? = null,
                      var angry : Int ? = null,
                      var disgust : Int? = null,
                      var fear : Int? = null,
                      var netural:Int? = null,
                      var total:Int?= null,
                      var visitTime : LocalDateTime? = null

 ) {
 @Id
 @GeneratedValue
 var id:Long?=null



}