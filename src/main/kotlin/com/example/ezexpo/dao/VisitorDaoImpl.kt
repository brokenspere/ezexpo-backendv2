package com.example.ezexpo.dao

import com.example.ezexpo.entity.Visitor
import com.example.ezexpo.repositoy.VisitorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class VisitorDaoImpl : VisitorDao{
    @Autowired
    lateinit var visitorRepository: VisitorRepository
    override fun save(visitor: Visitor): Visitor {
        return visitorRepository.save(visitor)
    }

}