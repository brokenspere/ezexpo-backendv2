package com.example.ezexpo.dao

import com.example.ezexpo.entity.BoothOwner
import com.example.ezexpo.entity.User
import com.example.ezexpo.entity.UserRole
import com.example.ezexpo.repositoy.BoothOwnerRepository
import com.example.ezexpo.repositoy.UserRepositoryObj
import com.example.ezexpo.security.entity.Authority
import com.example.ezexpo.security.entity.AuthorityName
import com.example.ezexpo.security.entity.JwtUser
import com.example.ezexpo.security.repository.AuthorityRepository
import com.example.ezexpo.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository

@Repository
class UserDaoImpl : UserDao {



    @Autowired
    lateinit var userRepositoryObj: UserRepositoryObj
    @Autowired
    lateinit var authorityRepository:AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var boothOwnerRepository: BoothOwnerRepository


    override fun getBoothDetailByUserId(id: Long): User? {
        return userRepositoryObj.findById(id).orElse(null)
    }
    override fun getUserByUsernameAndPassword(username: String, password: String): User? {
        return userRepositoryObj.getUserByUsernameAndPassword(username,password)
    }
    override fun save(user: User): User {
        val auth1 = Authority(name = AuthorityName.ROLE_BOOTHOWNER)
        authorityRepository.save(auth1)
        val encoder = BCryptPasswordEncoder()
        val boothOwn = user
        val boothJwt = JwtUser(
                username = boothOwn.username,
                password = encoder.encode(boothOwn.password),
                email = boothOwn.email,
                securityQuestion = boothOwn.securityQuestion,
                securityAnswer = boothOwn.securityAnswer,
                phoneNumber = boothOwn.phoneNumber,
                enabled = true,
                firstname = boothOwn.firstName,
                lastname = boothOwn.lastName

        )
        userRepositoryObj.save(boothOwn)
        userRepository.save(boothJwt)
        boothOwn.jwtUser = boothJwt
        boothJwt.user = boothOwn
        boothJwt.authorities.add(auth1)
        return userRepositoryObj.save(user)
    }
    override fun getUserByEmail(email: String): User? {
        return userRepositoryObj.getUserByEmail(email)
    }
    override fun getUserByRole(roleEnum: UserRole): List<User> {
        return userRepositoryObj.findByUserRole(roleEnum)
    }
    override fun getUserById(id: Long): User? {
        return userRepositoryObj.findById(id).orElse(null)
    }
    override fun saveTest(user: BoothOwner): BoothOwner {
        val auth1 = Authority(name = AuthorityName.ROLE_BOOTHOWNER)
        authorityRepository.save(auth1)
        val encoder = BCryptPasswordEncoder()
        val boothOwn = user
        val boothJwt = JwtUser(
                username = boothOwn.username,
                password = encoder.encode(boothOwn.password),
                email = boothOwn.email,
                securityQuestion = boothOwn.securityQuestion,
                securityAnswer = boothOwn.securityAnswer,
                phoneNumber = boothOwn.phoneNumber,
                enabled = true,
                firstname = boothOwn.firstName,
                lastname = boothOwn.lastName
        )
        boothOwnerRepository.save(boothOwn)
        userRepository.save(boothJwt)
        boothOwn.jwtUser = boothJwt
        boothJwt.user = boothOwn
        boothJwt.authorities.add(auth1)
        return boothOwnerRepository.save(user)
    }
}