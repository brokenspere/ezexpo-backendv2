package com.example.ezexpo.dao

import com.example.ezexpo.entity.BoothOwner
import com.example.ezexpo.entity.User
import com.example.ezexpo.entity.UserRole
import java.lang.Enum

interface UserDao {
     fun getBoothDetailByUserId(id: Long): User?
     fun getUserByUsernameAndPassword(username: String, password: String): User?
     fun save(user: User):User
     fun getUserByEmail(email: String): User?
     //fun getAllUser(): List<User>
     fun getUserById(id: Long): User?
     fun getUserByRole(roleEnum: UserRole): List<User>
     fun saveTest(user: BoothOwner): BoothOwner
}