package com.example.ezexpo.dao

import com.example.ezexpo.entity.VisitorRegister

interface VisitorRegisterDao {
    fun getAllvisitor(): List<VisitorRegister>
    fun save(visitor: VisitorRegister): VisitorRegister
}