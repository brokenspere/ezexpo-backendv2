package com.example.ezexpo.dao

import com.example.ezexpo.entity.VisitorRegister
import com.example.ezexpo.repositoy.VisitorRegisterRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class VisitorRegisterDaoImpl:VisitorRegisterDao {


    @Autowired
    lateinit var visitorRegisterRepository: VisitorRegisterRepository
    override fun getAllvisitor(): List<VisitorRegister> {
        return visitorRegisterRepository.findAll().filterIsInstance(VisitorRegister::class.java)
    }
    override fun save(visitor: VisitorRegister): VisitorRegister {
        return visitorRegisterRepository.save(visitor)
    }

}