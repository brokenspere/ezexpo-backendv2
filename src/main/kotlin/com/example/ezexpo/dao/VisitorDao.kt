package com.example.ezexpo.dao

import com.example.ezexpo.entity.Visitor

interface VisitorDao {
    fun save(visitor: Visitor): Visitor
}