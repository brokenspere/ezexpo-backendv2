package com.example.ezexpo.repositoy

import com.example.ezexpo.entity.SurveyAnswer
import com.example.ezexpo.entity.User
import org.springframework.data.repository.CrudRepository

interface SurveyAnswerRepository  : CrudRepository<SurveyAnswer, Long> {

}