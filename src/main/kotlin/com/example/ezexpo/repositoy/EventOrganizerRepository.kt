package com.example.ezexpo.repositoy

import com.example.ezexpo.entity.EventOrganizer
import com.example.ezexpo.entity.User
import org.springframework.data.repository.CrudRepository

interface EventOrganizerRepository  : CrudRepository<EventOrganizer, Long> {
}