package com.example.ezexpo.controller

import com.example.ezexpo.entity.BoothOwner
import com.example.ezexpo.service.BoothOwnerService
import com.example.ezexpo.service.UserService
import com.example.ezexpo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController

class BoothOwnerController {
    @Autowired
    lateinit var boothOwnerService: BoothOwnerService
    @Autowired
    lateinit var userService: UserService


    @GetMapping("/booth/userId")
    fun getBoothDetailByUserId(@RequestParam("id") id: String): ResponseEntity<Any> {
        try {
            val output = MapperUtil.INSTANCE.mapBoothOwnerDto(boothOwnerService.getBoothDetailById(id.toLong())!!)
            output?.let { return ResponseEntity.ok(it) }
            return ResponseEntity.ok("not found")
        } catch (n: KotlinNullPointerException) {
            return ResponseEntity.ok("not found")
        } catch (e: NumberFormatException) {
            return ResponseEntity.ok("invalid format")
        }
    }

//    @PostMapping("/addBooth")
//    fun addBoothOwner(@RequestBody booth :BoothOwner): ResponseEntity<Any>{
//        val output = MapperUtil.INSTANCE.mapBoothOwnerDto(boothOwnerService.save(booth))
//        output?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.ok("false")
//    }

}