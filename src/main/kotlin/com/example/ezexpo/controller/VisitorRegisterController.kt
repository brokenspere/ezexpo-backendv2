package com.example.ezexpo.controller


import com.example.ezexpo.entity.SurveyQuestion
import com.example.ezexpo.entity.VisitorRegister
import com.example.ezexpo.service.VisitorRegisterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

@RestController
class VisitorRegisterController {
    @Autowired
    lateinit var visitorRegisterService: VisitorRegisterService

    @GetMapping("/visitor")
    fun getAllVisitor(): ResponseEntity<Any> {
        val output = visitorRegisterService.getAllVisitor()
        return ResponseEntity.ok(output)
    }

    @PostMapping("/addVisitor")
    fun addVisitor(@RequestBody visitor : VisitorRegister):ResponseEntity<Any>{
        if(visitor.name == null || visitor.email == null || visitor.phoneNumber == null ){
            return ResponseEntity.ok("not input all required information")
        }
        try{
            var year:String = Calendar.getInstance().get(Calendar.YEAR).toString()
            var month:String = (Calendar.getInstance().get(Calendar.MONTH)+1).toString()
            var dateOfMonth:Int=Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
            var hour:String=Calendar.getInstance().get(Calendar.HOUR_OF_DAY).toString()
            var min:Int=Calendar.getInstance().get(Calendar.MINUTE)
            var dateStr:String
            var minStr:String
            if (dateOfMonth<10){
                dateStr = "0"+dateOfMonth.toString()
            }
            else{
                dateStr = dateOfMonth.toString()
            }
            if (min<10){
                minStr = "0"+min.toString()
            }
            else{
                minStr = min.toString()
            }

            var a:String = year+"-"+month+"-"+dateStr+" "+hour+":"+minStr
            var formatter:DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
            var dateTime:LocalDateTime = LocalDateTime.parse(a,formatter)
            visitor.registerTime = dateTime
            val output = visitorRegisterService.save(visitor)
            output?.let{
                if(it.name == null || it.email == null || it.phoneNumber == null  ){

                    return ResponseEntity.ok("not input all required information")
                }
                return ResponseEntity.ok(it)}

        }catch (e: KotlinNullPointerException){
            return ResponseEntity.ok("false")
        }
    }

}