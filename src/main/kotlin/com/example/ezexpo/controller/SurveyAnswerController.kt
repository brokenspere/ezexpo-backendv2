package com.example.ezexpo.controller

import com.example.ezexpo.entity.SurveyAnswer
import com.example.ezexpo.service.SurveyAnswerService
import com.example.ezexpo.service.SurveyQuestionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class SurveyAnswerController {
    @Autowired
    lateinit var surveyAnswerService: SurveyAnswerService

    @PostMapping("/answerSurvey")
    fun submitSurvey(@RequestBody answer : SurveyAnswer,@RequestParam("id")id:Long): ResponseEntity<Any>{
        try {
            val output = surveyAnswerService.save(answer,id)
            output?.let{
                if(it.answer1 == null || it.answer2 == null || it.answer3 == null || it.answer4 ==null || it.answer5 ==null){
                    return ResponseEntity.ok("not input all required information")
                }
                return ResponseEntity.ok(it)}
        }catch (e:KotlinNullPointerException){
            return ResponseEntity.ok("false")
        }
    }
}