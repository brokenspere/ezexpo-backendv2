package com.example.ezexpo.controller

import com.example.ezexpo.service.AmazonClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import javax.xml.ws.http.HTTPException

@RestController
class BucketController {
    @Autowired
    lateinit var amazonClient: AmazonClient


    @PostMapping("/uploadFile")
    fun uploadFile(@RequestPart(value = "file")file: MultipartFile?):ResponseEntity<*>{
            if(file== null){
                return ResponseEntity.ok("error")
            }else{
                return ResponseEntity.ok(this.amazonClient.uploadFile(file!!))
            }
    }

}