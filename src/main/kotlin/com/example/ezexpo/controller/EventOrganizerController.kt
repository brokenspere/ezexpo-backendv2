package com.example.ezexpo.controller

import com.example.ezexpo.entity.EventOrganizer
import com.example.ezexpo.entity.SurveyQuestion
import com.example.ezexpo.entity.VisitorRegister
import com.example.ezexpo.entity.dto.EventOrganizerDto
import com.example.ezexpo.entity.dto.displayUser
import com.example.ezexpo.repositoy.EventOrganizerRepository
import com.example.ezexpo.service.BoothOwnerService
import com.example.ezexpo.service.EventOrganizerService
import com.example.ezexpo.service.SurveyQuestionService
import com.example.ezexpo.service.UserService
import com.example.ezexpo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.NumberFormatException


@RestController
class EventOrganizerController {
    @Autowired
    lateinit var boothOwnerService: BoothOwnerService

    @Autowired
    lateinit var surveyQuestionService: SurveyQuestionService

    @Autowired
    lateinit var eventOrganizerRepository: EventOrganizerRepository

    @Autowired
    lateinit var eventOrganizerService: EventOrganizerService

    @GetMapping("/booth")
    fun getAllBoothDetail():ResponseEntity<Any>{
        val output = boothOwnerService.getAllBoothDetail()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapBoothOwnerDto(output))
    }

    @GetMapping("/survey")
    fun getAllSurvey():ResponseEntity<Any>{
        val output = surveyQuestionService.getAllSurvey()
        return ResponseEntity.ok(output)
    }

    @GetMapping("/survey/id")
    fun getSurveyById(@RequestParam("id")id:String):ResponseEntity<Any>{
        try{
            val output = surveyQuestionService.getSurveybyId(id.toLong()!!)
            output?.let{return ResponseEntity.ok(it)}
            return ResponseEntity.ok("not found")
        }catch (e:KotlinNullPointerException){
            return ResponseEntity.ok("not found")
        }catch (n:NumberFormatException){
            return ResponseEntity.ok("invalid format")
        }

    }

    @PostMapping("/createSurvey")
    fun createNewSurvey(@RequestBody survey : SurveyQuestion):ResponseEntity<Any>{
        try{
            val output = surveyQuestionService.save(survey)
            output?.let{
                if(it.surveyName == null || it.surveyDescription == null || it.question1 == null || it.question2 == null || it.question3 == null || it.question4 == null || it.question5 == null){
                    return ResponseEntity.ok("not input all required information")
                }
                return ResponseEntity.ok(it)}

        }catch (e: KotlinNullPointerException){
            return ResponseEntity.ok("false")
        }
    }

    @GetMapping("/announcement")
    fun getAnnouncement():ResponseEntity<Any>{
        val output = eventOrganizerRepository.findAll().filterIsInstance(EventOrganizer::class.java)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapEvenOrganizerDto(output))
    }

    @GetMapping("/announcement/id")
    fun getAnnouncementById(@RequestParam("id")id:String):ResponseEntity<Any>{
        try{
            val output = MapperUtil.INSTANCE.mapEventOrganizerDto(eventOrganizerRepository.findById(id.toLong()).orElse(null))
             output?.let { return ResponseEntity.ok(it) }
            return ResponseEntity.ok("not found")
        }catch (e:KotlinNullPointerException){
            return ResponseEntity.ok("not found")
        }catch (n:NumberFormatException){
            return ResponseEntity.ok("invalid format")
        }
    }


    @PutMapping("/updateAnnouncement")
    fun updateAnnouncement(@RequestParam("userid")id:String?,
                   @RequestBody event: EventOrganizer):ResponseEntity<Any>{
        try{
            if(id == null){
                return ResponseEntity.ok("invalid format")
            }
        }catch (n:NumberFormatException){
            return ResponseEntity.ok("invalid format")
        }
        event.id=id.toLong()!!
        val output = MapperUtil.INSTANCE.mapEventOrganizerDto(eventOrganizerService.update(event))
        output?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.ok("not found ")

    }

    @GetMapping("/getEvent")
    fun getEventOr():ResponseEntity<Any>{
        return ResponseEntity.ok(eventOrganizerRepository.findAll().filterIsInstance(EventOrganizer::class.java))
    }








}