package com.example.ezexpo.controller

import com.example.ezexpo.service.UploadFileServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

@RestController
class UploadFileController {
    @Autowired
    lateinit var uploadFileServiceImpl: UploadFileServiceImpl

    @PostMapping("/getEmotion")
    fun uploadFile(@RequestPart(value = "file")file:MultipartFile): ResponseEntity<*> {
           
            return ResponseEntity.ok(this.uploadFileServiceImpl.uploadFile(file))
    }
}