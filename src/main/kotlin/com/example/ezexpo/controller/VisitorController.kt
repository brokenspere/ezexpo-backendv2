package com.example.ezexpo.controller

import com.example.ezexpo.entity.Visitor
import com.example.ezexpo.service.VisitorService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


@RestController
class VisitorController {
    @Autowired
    lateinit var  visitorService: VisitorService

    @PostMapping("/addVisitorEmotion")
    fun addEmotion(@RequestBody visitor:Visitor,@RequestParam("id")id:Long): ResponseEntity<Any>{
        val output = visitorService.save(visitor,id)
        output.let{ if(it.angry == null || it.disgust == null || it.fear == null || it.happy == null || it.netural == null || it.sad == null || it.surprise == null || it.total == null || it.visitTime == null){
            return ResponseEntity.ok("not input all required information")
        }
            return ResponseEntity.ok(it)}
        return ResponseEntity.ok("false")
    }


}