package com.example.ezexpo.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@EnableWebMvc
class WebConfig: WebMvcConfigurer {

    @Override
     override fun addCorsMappings( registry: CorsRegistry){
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:3000", "http://localhost:5000")
//               .allowedOrigins("http://localhost:5000")
                .allowedMethods("PUT","DELETE","POST","GET","HEAD","OPTIONS")
    }

}