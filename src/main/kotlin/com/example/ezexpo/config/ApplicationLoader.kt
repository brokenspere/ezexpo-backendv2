package com.example.ezexpo.config

import com.example.ezexpo.dao.VisitorRegisterDaoImpl
import com.example.ezexpo.entity.*
import com.example.ezexpo.repositoy.*
import com.example.ezexpo.repositoy.BoothOwnerRepository
import com.example.ezexpo.repositoy.VisitorRepository
import com.example.ezexpo.security.entity.Authority
import com.example.ezexpo.security.entity.AuthorityName
import com.example.ezexpo.security.entity.JwtUser
import com.example.ezexpo.security.repository.AuthorityRepository
import com.example.ezexpo.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.sql.Timestamp
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import javax.transaction.Transactional


@Component
class ApplicationLoader : ApplicationRunner {

    @Autowired
    lateinit var boothOwnerRepository: BoothOwnerRepository
    @Autowired
    lateinit var visitorRepository: VisitorRepository
    @Autowired
    lateinit var userRepositoryObj: UserRepositoryObj
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var jwtUserRepository: UserRepository
    @Autowired
    lateinit var eventOrganizerRepository: EventOrganizerRepository
    @Autowired
    lateinit var surveyQuestionRepository: SurveyQuestionRepository
    @Autowired
    lateinit var surveyAnswerRepository: SurveyAnswerRepository
    @Autowired
    lateinit var visitorRegisterRepository: VisitorRegisterRepository

    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_BOOTHOWNER)
        val auth2 = Authority(name = AuthorityName.ROLE_EVENTORGANIZER)

        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        val encoder = BCryptPasswordEncoder()
        val userTest = User("abc","abc","a@b.com")
        val userJwt = JwtUser(
                username = "abc",
                password = encoder.encode("123456"),
                email = userTest.email,
                enabled = true,
                firstname = userTest.firstName,
                lastname = userTest.lastName

        )
        jwtUserRepository.save(userJwt)
        userRepositoryObj.save(userTest)
        userTest.jwtUser = userJwt
        userJwt.user = userTest
        userJwt.authorities.add(auth1)
        userJwt.authorities.add(auth2)
    }

    @Transactional
    override fun run(args: ApplicationArguments?) {
        val encoder = BCryptPasswordEncoder()
        val auth1 = Authority(name = AuthorityName.ROLE_BOOTHOWNER)
        val auth2 = Authority(name = AuthorityName.ROLE_EVENTORGANIZER)

        authorityRepository.save(auth1)
        authorityRepository.save(auth2)

        var emoTime1 = visitorRepository.save(Visitor(5, 6, 8, 3, 7, 5, 10, 44,  LocalDateTime.of(2019,11,8,9,0)))
        var emoTime2 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 33, LocalDateTime.of(2019,11,8,11,0)))
        var emoTime3 = visitorRepository.save(Visitor(3, 4, 1, 2, 7, 1, 12, 30, LocalDateTime.of(2019,11,8,13,0)))
        var emoTime4 = visitorRepository.save(Visitor(5, 1, 3, 1, 1, 2, 15, 28, LocalDateTime.of(2019,11,8,15,0)))
        var emoTime5 = visitorRepository.save(Visitor(4, 2, 5, 2, 1, 1, 15, 30, LocalDateTime.of(2019,11,8,17,0)))

        var emoTime6 = visitorRepository.save(Visitor(4, 3, 8, 2, 7, 5, 15, 44,  LocalDateTime.of(2019,11,8,9,0)))
        var emoTime7 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 33,  LocalDateTime.of(2019,11,8,11,0)))
        var emoTime8 = visitorRepository.save(Visitor(17, 1, 7, 3, 2, 1, 11, 42, LocalDateTime.of(2019,11,8,13,0)))
        var emoTime9 = visitorRepository.save(Visitor(5, 1, 3, 1, 1, 2, 17, 30, LocalDateTime.of(2019,11,8,15,0)))
        var emoTime10 = visitorRepository.save(Visitor(4, 2, 5, 2, 4, 1, 15, 55, LocalDateTime.of(2019,11,8,17,0)))

        var emoTime11 = visitorRepository.save(Visitor(8, 1, 10, 1, 3, 2, 12, 37, LocalDateTime.of(2019,11,8,9,0)))
        var emoTime12 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 33, LocalDateTime.of(2019,11,8,11,0)))
        var emoTime13 = visitorRepository.save(Visitor(12, 3, 6, 3, 4, 1, 11, 40,  LocalDateTime.of(2019,11,8,13,0)))
        var emoTime14 = visitorRepository.save(Visitor(5, 1, 3, 1, 1, 2, 17, 30, LocalDateTime.of(2019,11,8,15,0)))
        var emoTime15 = visitorRepository.save(Visitor(4, 2, 5, 6, 5, 1, 20, 33,  LocalDateTime.of(2019,11,8,17,0)))

        var emoTime16 = visitorRepository.save(Visitor(4, 3, 8, 2, 7, 5, 15, 44, LocalDateTime.of(2019,11,8,9,0)))
        var emoTime17 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 33,  LocalDateTime.of(2019,11,8,11,0)))
        var emoTime18 = visitorRepository.save(Visitor(15, 1, 7, 3, 2, 1, 11, 40,  LocalDateTime.of(2019,11,8,13,0)))
        var emoTime19 = visitorRepository.save(Visitor(7, 2, 5, 1, 2, 1, 21, 29, LocalDateTime.of(2019,11,8,15,0)))
        var emoTime20 = visitorRepository.save(Visitor(4, 2, 5, 2, 4, 1, 15, 33, LocalDateTime.of(2019,11,8,17,0)))

        var emoTime21 = visitorRepository.save(Visitor(6, 1, 7, 2, 5, 2, 20, 43, LocalDateTime.of(2019,11,8,9,0)))
        var emoTime22 = visitorRepository.save(Visitor(10, 2, 7, 1, 2, 1, 10, 33, LocalDateTime.of(2019,11,8,11,0)))
        var emoTime23 = visitorRepository.save(Visitor(17, 1, 7, 3, 2, 1, 11, 41,  LocalDateTime.of(2019,11,8,13,0)))
        var emoTime24 = visitorRepository.save(Visitor(5, 1, 3, 1, 1, 2, 17, 30, LocalDateTime.of(2019,11,8,15,0)))
        var emoTime25 = visitorRepository.save(Visitor(4, 2, 5, 2, 4, 1, 15, 33,  LocalDateTime.of(2019,11,8,17,0)))

        var survey1 = surveyQuestionRepository.save(SurveyQuestion("Thailand game show 2019","Survey for gather visitor satisfaction in Thailand game show 2019","What is your level of satisfaction for this exhibition?","How convenience of our exhibition hours?","How was staff’s friendliness and courtesy?","How likely are you to recommend this exhibition to a friend?","Are you likely to participate in one of our exhibition in the future?"))
        var survey2 = surveyQuestionRepository.save(SurveyQuestion("CMU ambassador 2019","Survey for gather visitor satisfaction in CMU ambassador 2019","What is your level of satisfaction for this exhibition?","How convenience of our exhibition hours?","How was staff’s friendliness and courtesy?","How likely are you to recommend this exhibition to a friend?","Are you likely to participate in one of our exhibition in the future?"))
        var survey3 = surveyQuestionRepository.save(SurveyQuestion("CMU trekking 2019","Survey for gather visitor satisfaction in CMU trekking 2019","What is your level of satisfaction for this exhibition?","How convenience of our exhibition hours?","How was staff’s friendliness and courtesy?","How likely are you to recommend this exhibition to a friend?","Are you likely to participate in one of our exhibition in the future?"))
        var survey4 = surveyQuestionRepository.save(SurveyQuestion("U-league 2019","Survey for gather visitor satisfaction in U-league 2019","What is your level of satisfaction for this exhibition?","How convenience of our exhibition hours?","How was staff’s friendliness and courtesy?","How likely are you to recommend this exhibition to a friend?","Are you likely to participate in one of our exhibition in the future?"))
        var survey5 = surveyQuestionRepository.save(SurveyQuestion("The international 2019","Survey for gather visitor satisfaction in The international 2019","What is your level of satisfaction for this exhibition?","How convenience of our exhibition hours?","How was staff’s friendliness and courtesy?","How likely are you to recommend this exhibition to a friend?","Are you likely to participate in one of our exhibition in the future?"))


        var answer1 = surveyAnswerRepository.save(SurveyAnswer("3","4","3","4","2",null))
        var answer2 = surveyAnswerRepository.save(SurveyAnswer("3","5","3","1","2","this expo very bored u have to have some entertainment in expo"))
        var answer3 = surveyAnswerRepository.save(SurveyAnswer("1","2","1","4","5","i like so much, how can i know when have expo like this again"))
        var answer4 = surveyAnswerRepository.save(SurveyAnswer("4","3","2","2","5","next time can we allow pet in expo ?"))
        var answer5 = surveyAnswerRepository.save(SurveyAnswer("4","1","4","2","3","event on stage is too much loud can you turn volume down next time?"))

        var answer6 = surveyAnswerRepository.save(SurveyAnswer("3","4","3","4","2","not bad"))
        var answer7 = surveyAnswerRepository.save(SurveyAnswer("3","5","3","1","2","great event"))
        var answer8 = surveyAnswerRepository.save(SurveyAnswer("1","2","1","4","5","No parking lot"))
        var answer9= surveyAnswerRepository.save(SurveyAnswer("4","3","2","2","5","Staff is friendly"))
        var answer10 = surveyAnswerRepository.save(SurveyAnswer("4","1","4","2","3","No toilet??"))

        var answer11 = surveyAnswerRepository.save(SurveyAnswer("3","4","3","4","2","average is good"))
        var answer12 = surveyAnswerRepository.save(SurveyAnswer("3","5","3","1","2","Come to Chaing mai next time"))
        var answer13 = surveyAnswerRepository.save(SurveyAnswer("1","2","1","4","5","don't have enough seat"))
        var answer14= surveyAnswerRepository.save(SurveyAnswer("4","3","2","2","5","difficult to come here"))
        var answer15 = surveyAnswerRepository.save(SurveyAnswer("4","1","4","2","3","good"))

        var answer16 = surveyAnswerRepository.save(SurveyAnswer("3","4","3","4","2","not bad"))
        var answer17 = surveyAnswerRepository.save(SurveyAnswer("3","5","3","1","2","No toilet??"))
        var answer18 = surveyAnswerRepository.save(SurveyAnswer("1","2","1","4","5","difficult to come here"))
        var answer19= surveyAnswerRepository.save(SurveyAnswer("4","3","2","2","5","food is expensive"))
        var answer20 = surveyAnswerRepository.save(SurveyAnswer("4","1","4","2","3","product is good"))

        var answer21 = surveyAnswerRepository.save(SurveyAnswer("3","4","3","4","2","Staff is friendly"))
        var answer22 = surveyAnswerRepository.save(SurveyAnswer("3","5","3","1","2","great event"))
        var answer23 = surveyAnswerRepository.save(SurveyAnswer("1","2","1","4","5","difficult to come here"))
        var answer24= surveyAnswerRepository.save(SurveyAnswer("4","3","2","2","5","this expo very bored u have to have some entertainment in expo"))
        var answer25 = surveyAnswerRepository.save(SurveyAnswer("4","1","4","2","3","good"))

        var visitor1 = visitorRegisterRepository.save(VisitorRegister("Harry Potter","harry@gmail.com","0910288745", LocalDateTime.of(2019,11,8,15,20)))
        var visitor2 = visitorRegisterRepository.save(VisitorRegister("Pravith Wongsuwan","pravith@gmail.com","0842227896",LocalDateTime.of(2019,11,8,15,30)))
        var visitor3 = visitorRegisterRepository.save(VisitorRegister("Natasha Romanorv","natasha@gmail.com","0852488965",LocalDateTime.of(2019,11,8,16,22)))
        var visitor4 = visitorRegisterRepository.save(VisitorRegister("Louis Wongsuwan ","Louis@gmail.com","0852411465",LocalDateTime.of(2019,11,8,16,22)))
        var visitor5 = visitorRegisterRepository.save(VisitorRegister("Josiah Romanorv ","Josiah@gmail.com","0852488775",LocalDateTime.of(2019,11,8,16,22)))

        var visitor6 = visitorRegisterRepository.save(VisitorRegister("Charlie Potter","Chas@gmail.com","0910852745", LocalDateTime.of(2019,11,8,17,20)))
        var visitor7 = visitorRegisterRepository.save(VisitorRegister("Eddie Wongsuwan","Eddie@gmail.com","0844857896",LocalDateTime.of(2019,11,8,20,30)))
        var visitor8 = visitorRegisterRepository.save(VisitorRegister("Robin Romanorv","Lukas@gmail.com","0852088965",LocalDateTime.of(2019,11,8,20,30)))
        var visitor9 = visitorRegisterRepository.save(VisitorRegister("Nora Romanorv","Nora@gmail.com","0852365965",LocalDateTime.of(2019,11,8,22,22)))
        var visitor10 = visitorRegisterRepository.save(VisitorRegister("Cory Romanorv","Cory@gmail.com","0814588965",LocalDateTime.of(2019,11,8,22,22)))

        var visitor11 = visitorRegisterRepository.save(VisitorRegister("Sonny Potter","Sonny@gmail.com","0915558745", LocalDateTime.of(2019,11,9,8,20)))
        var visitor12 = visitorRegisterRepository.save(VisitorRegister("Owen Wongsuwan","Owen@gmail.com","0842887896",LocalDateTime.of(2019,11,9,8,20)))
        var visitor13 = visitorRegisterRepository.save(VisitorRegister("Edgar Romanorv","Edgar@gmail.com","0852458665",LocalDateTime.of(2019,11,9,10,22)))
        var visitor14 = visitorRegisterRepository.save(VisitorRegister("Theodore Romanorv","Theodore@gmail.com","0855218965",LocalDateTime.of(2019,11,9,11,22)))
        var visitor15 = visitorRegisterRepository.save(VisitorRegister("George Romanorv","George@gmail.com","0852487845",LocalDateTime.of(2019,11,9,13,22)))

        var visitor16 = visitorRegisterRepository.save(VisitorRegister("Alan  Potter","Alan@gmail.com","0902285555", LocalDateTime.of(2019,11,9,19,20)))
        var visitor17 = visitorRegisterRepository.save(VisitorRegister("Marc Wongsuwan","Marc@gmail.com","0896586896",LocalDateTime.of(2019,11,10,15,30)))
        var visitor18 = visitorRegisterRepository.save(VisitorRegister("Hamzah  Romanorv","Hamzah@gmail.com","0820388965",LocalDateTime.of(2019,11,10,16,22)))
        var visitor19 = visitorRegisterRepository.save(VisitorRegister("Tobias Romanorv","Tobias@gmail.com","0852485465",LocalDateTime.of(2019,11,10,16,22)))
        var visitor20 = visitorRegisterRepository.save(VisitorRegister("Casey Romanorv","Casey@gmail.com","085265965",LocalDateTime.of(2019,11,10,16,22)))


        var boothOwn1 = boothOwnerRepository.save(BoothOwner("Prayuth", "Junungkarn", "prayuth@gmail.com", "prayuth123", "1212312121","0805243088","What is the name of your favorite childhood friend?", "Tony", UserRole.BOOTHOWNER,"Submarine booth"))
        val userJwt1 = JwtUser(
                username = boothOwn1.username,
                password = encoder.encode(boothOwn1.password),
                email = boothOwn1.email,
                enabled = true,
                firstname = boothOwn1.firstName,
                lastname = boothOwn1.lastName

        )
        userRepositoryObj.save(boothOwn1)
        boothOwn1.jwtUser = userJwt1
        userJwt1.user = boothOwn1
        jwtUserRepository.save(userJwt1)
        userJwt1.authorities.add(auth1)
        var boothOwn2 = boothOwnerRepository.save(BoothOwner("Nattapat", "Tamtrakool", "nattapat@gmail.com", "nattapat123", "123456","0815547632", "What school did you attend for sixth grade?", "Varee",UserRole.BOOTHOWNER,"Dan Booth"))
        val userJwt2 = JwtUser(
                username = boothOwn2.username,
                password = encoder.encode(boothOwn2.password),
                email = boothOwn2.email,
                enabled = true,
                firstname = boothOwn2.firstName,
                lastname = boothOwn2.lastName

        )
        userRepositoryObj.save(boothOwn2)
        boothOwn2.jwtUser = userJwt2
        userJwt2.user = boothOwn2
        jwtUserRepository.save(userJwt2)
        userJwt2.authorities.add(auth1)
        var boothOwn3 = boothOwnerRepository.save(BoothOwner("Tony", "Stark", "tony@gmail.com", "tony123", "123456","0910744563", "What was your childhood nickname?", "tony",UserRole.BOOTHOWNER,"Ironman Booth"))
        val userJwt3 = JwtUser(
                username = boothOwn3.username,
                password = encoder.encode(boothOwn3.password),
                email = boothOwn3.email,
                enabled = true,
                firstname = boothOwn3.firstName,
                lastname = boothOwn3.lastName

        )
        userRepositoryObj.save(boothOwn3)
        boothOwn3.jwtUser = userJwt3
        userJwt3.user = boothOwn3
        jwtUserRepository.save(userJwt3)
        userJwt3.authorities.add(auth1)
        var boothOwn4 = boothOwnerRepository.save(BoothOwner("Steve", "Roger", "steve@gmail.com", "steve123", "123456","0862257841", "What is the country of your ultimate dream vacation?", "Japan",UserRole.BOOTHOWNER,"Sheild Booth"))
        val userJwt4 = JwtUser(
                username = boothOwn4.username,
                password = encoder.encode(boothOwn4.password),
                email = boothOwn4.email,
                enabled = true,
                firstname = boothOwn4.firstName,
                lastname = boothOwn4.lastName

        )

        userRepositoryObj.save(boothOwn4)
        boothOwn4.jwtUser = userJwt4
        userJwt4.user = boothOwn4
        jwtUserRepository.save(userJwt4)
        userJwt4.authorities.add(auth1)

        var boothOwn5 = boothOwnerRepository.save(BoothOwner("Thor", "Asgard", "thor@gmail.com", "thor123", "123456","0910844523", "Who was your childhood hero?", "Odin",UserRole.BOOTHOWNER,"Hammer Booth"))
        val userJwt5 = JwtUser(
                username = boothOwn5.username,
                password = encoder.encode(boothOwn5.password),
                email = boothOwn5.email,
                enabled = true,
                firstname = boothOwn5.firstName,
                lastname = boothOwn5.lastName

        )
        userRepositoryObj.save(boothOwn5)
        boothOwn5.jwtUser = userJwt5
        userJwt5.user = boothOwn5
        jwtUserRepository.save(userJwt5)
        userJwt5.authorities.add(auth1)

        var eventOr1 = eventOrganizerRepository.save(EventOrganizer("Abdul","Alan","Abdul@gmail.com","abdul123","123456","0814452365","What school did you attend for sixth grade?","Prince Royal",UserRole.EVENTORGANIZER,"Pre-register will end in 2 December 2019"))
        val userJwt6 = JwtUser(
                username = eventOr1.username,
                password = encoder.encode(eventOr1.password),
                email = eventOr1.email,
                enabled = true,
                firstname = eventOr1.firstName,
                lastname = eventOr1.lastName

        )
        userRepositoryObj.save(eventOr1)
        eventOr1.jwtUser = userJwt6
        userJwt6.user = eventOr1
        jwtUserRepository.save(userJwt6)
        userJwt6.authorities.add(auth2)
//        boothOwn1.visitor = mutableListOf()


        boothOwn1.visitor?.add(emoTime1)
        boothOwn1.visitor?.add(emoTime2)
        boothOwn1.visitor?.add(emoTime3)
        boothOwn1.visitor?.add(emoTime4)
        boothOwn1.visitor?.add(emoTime5)

        boothOwn2.visitor?.add(emoTime6)
        boothOwn2.visitor?.add(emoTime7)
        boothOwn2.visitor?.add(emoTime8)
        boothOwn2.visitor?.add(emoTime9)
        boothOwn2.visitor?.add(emoTime10)

        boothOwn3.visitor?.add(emoTime11)
        boothOwn3.visitor?.add(emoTime12)
        boothOwn3.visitor?.add(emoTime13)
        boothOwn3.visitor?.add(emoTime14)
        boothOwn3.visitor?.add(emoTime15)

        boothOwn4.visitor?.add(emoTime16)
        boothOwn4.visitor?.add(emoTime17)
        boothOwn4.visitor?.add(emoTime18)
        boothOwn4.visitor?.add(emoTime19)
        boothOwn4.visitor?.add(emoTime20)

        boothOwn5.visitor?.add(emoTime21)
        boothOwn5.visitor?.add(emoTime22)
        boothOwn5.visitor?.add(emoTime23)
        boothOwn5.visitor?.add(emoTime24)
        boothOwn5.visitor?.add(emoTime25)

        eventOr1.surveyQuestion?.add(survey1)
        survey1.surveyAnswer?.add(answer1)
        survey1.surveyAnswer?.add(answer2)
        survey1.surveyAnswer?.add(answer3)
        survey1.surveyAnswer?.add(answer4)
        survey1.surveyAnswer?.add(answer5)

        survey2.surveyAnswer?.add(answer6)
        survey2.surveyAnswer?.add(answer7)
        survey2.surveyAnswer?.add(answer8)
        survey2.surveyAnswer?.add(answer9)
        survey2.surveyAnswer?.add(answer10)

        survey3.surveyAnswer?.add(answer11)
        survey3.surveyAnswer?.add(answer12)
        survey3.surveyAnswer?.add(answer13)
        survey3.surveyAnswer?.add(answer14)
        survey3.surveyAnswer?.add(answer15)


        survey4.surveyAnswer?.add(answer16)
        survey4.surveyAnswer?.add(answer17)
        survey4.surveyAnswer?.add(answer18)
        survey4.surveyAnswer?.add(answer19)
        survey4.surveyAnswer?.add(answer20)

        survey5.surveyAnswer?.add(answer21)
        survey5.surveyAnswer?.add(answer22)
        survey5.surveyAnswer?.add(answer23)
        survey5.surveyAnswer?.add(answer24)
        survey5.surveyAnswer?.add(answer25)

        eventOr1.visitorRegister?.add(visitor1)
        eventOr1.visitorRegister?.add(visitor2)
        eventOr1.visitorRegister?.add(visitor3)
        eventOr1.visitorRegister?.add(visitor4)
        eventOr1.visitorRegister?.add(visitor5)

        eventOr1.visitorRegister?.add(visitor6)
        eventOr1.visitorRegister?.add(visitor7)
        eventOr1.visitorRegister?.add(visitor8)
        eventOr1.visitorRegister?.add(visitor9)
        eventOr1.visitorRegister?.add(visitor10)

        eventOr1.visitorRegister?.add(visitor11)
        eventOr1.visitorRegister?.add(visitor12)
        eventOr1.visitorRegister?.add(visitor13)
        eventOr1.visitorRegister?.add(visitor14)
        eventOr1.visitorRegister?.add(visitor15)

        eventOr1.visitorRegister?.add(visitor16)
        eventOr1.visitorRegister?.add(visitor17)
        eventOr1.visitorRegister?.add(visitor18)
        eventOr1.visitorRegister?.add(visitor19)
        eventOr1.visitorRegister?.add(visitor20)



//        eventOr1.surveyQuestion?.add(survey2)
//        survey2.surveyAnswer?.add(answer1)
//        survey2.surveyAnswer?.add(answer2)
//
//        eventOr1.surveyQuestion?.add(survey3)
//        survey3.surveyAnswer?.add(answer1)
//        survey3.surveyAnswer?.add(answer2)
//
//        eventOr1.surveyQuestion?.add(survey4)
//        survey4.surveyAnswer?.add(answer1)
//        survey4.surveyAnswer?.add(answer2)
//
//        eventOr1.surveyQuestion?.add(survey5)
//        survey5.surveyAnswer?.add(answer1)
//        survey5.surveyAnswer?.add(answer2)


      //  loadUsernameAndPassword()
    }

}

